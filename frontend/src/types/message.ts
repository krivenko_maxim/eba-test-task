type ChannelType = 'SMS' | 'VK' | 'Telegram' | 'WhatsApp';
type KeyboardType = 'inline' | 'standard';
type KeyboardButtonType = 'link' | 'fast-response';

interface KeyboardButton {
  text: string;
  type: KeyboardButtonType;
}

interface Message {
  type: ChannelType;
  text: string;
  keyboardType?: KeyboardType;
  buttons?: KeyboardButton[];
}

export type { Message, ChannelType, KeyboardType, KeyboardButton };
