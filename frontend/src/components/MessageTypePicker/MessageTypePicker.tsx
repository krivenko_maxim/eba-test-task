import { Switch, FormControlLabel } from '@mui/material';
import classes from './MessageTypePicker.module.css';
import { ChannelType } from '../../types/message';

interface MessageTypePickerProps {
  onChange: (type: ChannelType, value: boolean) => void;
  checkedArray: Record<ChannelType, boolean>;
}

const channels: ChannelType[] = ['VK', 'Telegram', 'SMS', 'WhatsApp'];

function MessageTypePicker({ checkedArray, onChange }: MessageTypePickerProps) {
  return (
    <div className={classes.grid}>
      {channels.map((channel) => (
        <FormControlLabel
          key={channel}
          label={channel}
          control={
            <Switch
              checked={checkedArray[channel]}
              onChange={(event) => onChange(channel, event.target.checked)}
            />
          }
        />
      ))}
    </div>
  );
}

export default MessageTypePicker;
