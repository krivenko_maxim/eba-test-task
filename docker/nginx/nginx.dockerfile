FROM node:20-alpine as build

WORKDIR /usr/app/nginx
ARG VITE_API_URL=http://127.0.0.1:3000
COPY ./frontend/package.json .

RUN npm install

COPY ./frontend .

RUN npm run build

FROM nginx:alpine
COPY --from=build /usr/app/nginx/dist /dist

RUN mkdir -p /var/logs/nginx/backend/ && rm /etc/nginx/conf.d/default.conf
COPY ./docker/nginx/nginx.conf /etc/nginx/conf.d/