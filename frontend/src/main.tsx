import ReactDOM from 'react-dom/client';
import App from './App.tsx';
import { QueryClient, QueryClientProvider } from 'react-query';
import { CustomToastContainer } from './components/CustomToastContainer/CustomToastContainer.tsx';
import 'react-toastify/ReactToastify.css';

const client = new QueryClient();

ReactDOM.createRoot(document.getElementById('root')!).render(
  <QueryClientProvider client={client}>
    <App />
    <CustomToastContainer />
  </QueryClientProvider>
);
