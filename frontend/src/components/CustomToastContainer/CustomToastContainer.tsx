import { ToastContainer } from 'react-toastify';

function CustomToastContainer() {
  return <ToastContainer position="bottom-left" autoClose={3000} />;
}

export { CustomToastContainer };
