type ChannelType = "SMS" | "VK" | "Telegram" | "WhatsApp";
type KeyboardType = "inline" | "standard";
type KeyboardButtonType = "link" | "fast-response";

interface MessageObject {
  type: ChannelType;
  text: string;
  keyboardType?: KeyboardType;
  buttons?: KeyboardButton[];
}

interface KeyboardButton {
  text: string;
  type: KeyboardButtonType;
}

interface RequestBody {
  username: string;
  chosenChannels: ChannelType[];
  messages: MessageObject[];
}

export type { RequestBody, ChannelType, MessageObject };
