import { ChannelType } from "../types/body.js";

const messageTypes: ChannelType[] = ["VK", "Telegram", "SMS", "WhatsApp"];

export { messageTypes };
