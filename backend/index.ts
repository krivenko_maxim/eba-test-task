import "dotenv/config";
import express, { Request, Response } from "express";
import cors from "cors";
import { sequelize } from "./db/init.js";
import { MessageType, User } from "./db/schema.js";
import DataController from "./controllers/dataController.js";
import { messageTypes } from "./constants/messageType.js";

const app = express();

app.use(cors());
app.use(express.json());

app.post("/data", DataController.create);

const port = process.env.BACKEND_PORT || 3000;

(async function () {
  try {
    await sequelize.sync({alter: true});
    await MessageType.bulkCreate(
      messageTypes.map((type) => ({
        message_type: type,
      }))
    );
    app.listen(port, () => console.log(`Server started on port ${port}`));
  } catch (e) {
    console.error(e);
  }
})();
