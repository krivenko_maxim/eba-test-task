import { Message } from './message';

interface CreateCampaignRequest {
  username: string;
  messages: Message[];
}

export type { CreateCampaignRequest };
