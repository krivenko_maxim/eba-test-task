import { Transaction } from "sequelize";
import { RequestBody } from "../types/body.js";
import { NextFunction, Request, Response } from "express";
import { sequelize } from "../db/init.js";
import { User, Message, KeyboardButton, MessageType } from "../db/schema.js";

class DataController {
  async create(
    req: Request<{}, {}, RequestBody>,
    res: Response,
    next: NextFunction
  ) {
    try {
      const body = req.body;
      await sequelize.transaction(async (t: Transaction) => {
        const [user] = await User.findOrCreate({
          where: {
            username: body.username,
          },
          transaction: t,
        });
        console.log(user)
        for (const message of body.messages) {
          const messageType = await MessageType.findOne({
            where: {
              message_type: message.type,
            },
            transaction: t,
          });
          console.log(messageType)
          const createdMessage = await Message.create(
            {
              user_id: user.id,
              text: message.text,
              keyboard_type: message.keyboardType,
              message_type_id: messageType?.id,
            },
            {
              transaction: t,
            }
          );
          console.log(createdMessage)
          if (message.buttons) {
            KeyboardButton.bulkCreate(
              message.buttons.map((button) => ({
                text: button.text,
                message_id: createdMessage.id,
              })),
              {
                transaction: t,
              }
            );
          }
        };
      });
      res.send({
        message: "Данные получены",
      });
    } catch (e) {
        console.log(e)
      res.status(422).send({
        message: "Данные неверны",
      });
    }
  }
}

export default new DataController();
