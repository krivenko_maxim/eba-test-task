import { useMutation } from 'react-query';
import { toast } from 'react-toastify';
import { customFetch } from './api/fetch';
import MessageTypePicker from './components/MessageTypePicker/MessageTypePicker';
import { useCallback, useState, useMemo } from 'react';
import { ChannelType, Message } from './types/message';
import MessageBuilder from './components/MessageBuilder/MessageBuilder';
import { produce } from 'immer';
import { Button, Container, Stack, Typography } from '@mui/material';
import {
  defaultMessageMetadata,
  keyboardAvailable,
} from './constants/messageMetadata';
import styles from './App.module.css';
import UsernamePicker from './components/UsernamePicker/UsernamePicker';
import { CreateCampaignRequest } from './types/api';

function App() {
  const [username, setUsername] = useState<string>('');

  const [chosenChannels, setChosenChannels] = useState<
    Record<ChannelType, boolean>
  >({
    SMS: false,
    Telegram: false,
    VK: false,
    WhatsApp: false,
  });

  const [messageMetadata, setMessageMetadata] = useState<Message[]>(
    defaultMessageMetadata
  );

  const { mutate: send, isLoading } = useMutation({
    mutationKey: ['data'],
    mutationFn: () =>
      customFetch<CreateCampaignRequest>({
        endpoint: 'data',
        data: {
          username: username,
          messages: messageMetadata.filter(
            (message) => chosenChannels[message.type]
          ),
        },
        method: 'POST',
      }),
    onSuccess: () => {
      toast('Отлично! Данные сохранены', { type: 'success' });
    },
    onError: () => {
      toast('Произошла ошибка, попробуйте позднее', { type: 'error' });
    },
  });

  const handleChosenChange = useCallback(
    (type: ChannelType, value: boolean) => {
      setChosenChannels((prev) => ({
        ...prev,
        [type]: value,
      }));
    },
    []
  );

  return (
    <main className={styles.main}>
      <Stack>
        <UsernamePicker username={username} onChangeUsername={setUsername} />
        <MessageTypePicker
          onChange={handleChosenChange}
          checkedArray={chosenChannels}
        />
      </Stack>
      <div className={styles.pickers}>
        <Typography>Выберите каналы для отправки сообщения: </Typography>
        <div className={styles.grid}>
          {Object.entries(chosenChannels).map((channel) => {
            const message = messageMetadata.find((m) => m.type === channel[0])!;
            return (
              <MessageBuilder
                key={channel[0]}
                disabled={!channel[1]}
                buttons={message.buttons}
                setButtons={(newButtons) =>
                  setMessageMetadata(
                    produce((draft) => {
                      draft.find((m) => m.type === channel[0])!.buttons =
                        newButtons;
                    })
                  )
                }
                keyboardAvailable={keyboardAvailable[channel[0] as ChannelType]}
                setKeyboardType={(type) =>
                  setMessageMetadata(
                    produce((draft) => {
                      draft.find((m) => m.type === channel[0])!.keyboardType =
                        type;
                    })
                  )
                }
                keyboardType={message.keyboardType}
                channel={channel[0]}
                messageText={message.text}
                onChangeMessage={(newMessage) =>
                  setMessageMetadata(
                    produce((draft) => {
                      draft.find((m) => m.type === channel[0])!.text =
                        newMessage;
                    })
                  )
                }
              />
            );
          })}
        </div>
        <Button
          disabled={isLoading}
          className={styles.btn}
          variant="contained"
          onClick={() => send()}
        >
          Сохранить
        </Button>
      </div>
    </main>
  );
}

export default App;
