interface FetchProps<T> extends RequestInit {
  endpoint: string;
  data: T;
}

const defaultOptions = {
  method: 'GET',
  mode: 'cors',
  credentials: 'omit',
  headers: {
    'Content-Type': 'application/json',
  },
} satisfies RequestInit;

const customFetch = async <T>({
  endpoint,
  method = 'GET',
  data,
}: FetchProps<T>) => {
  const res = await fetch(`${import.meta.env.VITE_API_URL}/${endpoint}`, {
    ...defaultOptions,
    method,
    body: JSON.stringify(data),
  });

  if (!res.ok) {
    throw new Error('Error!');
  }

  return res.json();
};

export { customFetch };
