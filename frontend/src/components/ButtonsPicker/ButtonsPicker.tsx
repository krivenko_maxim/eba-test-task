import { useState } from 'react';
import { KeyboardButton } from '../../types/message';
import {
  Modal,
  Button,
  Typography,
  Box,
  TextField,
  Container,
  Stack,
  FormControlLabel,
  Switch,
} from '@mui/material';
import { CheckSharp, Add } from '@mui/icons-material';
import styles from './ButtonsPicker.module.css';
import { produce } from 'immer';

interface ButtonPickerProps {
  buttons: KeyboardButton[];
  setButtons?: (newButtons: KeyboardButton[]) => void;
}

function ButtonsPicker({ buttons, setButtons }: ButtonPickerProps) {
  const [isModalOpen, setModalOpen] = useState<boolean>(false);
  const [chosenButtons, setChosenButtons] = useState<KeyboardButton[]>(buttons);

  return (
    <>
      <Button variant="contained" onClick={() => setModalOpen(true)}>
        Выбрать кнопки
      </Button>
      <Modal open={isModalOpen} onClose={() => setModalOpen(false)}>
        <Box className={styles.box}>
          <Typography variant="h4">Выберите кнопки на клавиатуре</Typography>
          <Stack gap={1} marginY={2} className={styles.buttons_stack}>
            {chosenButtons.map((chosenButton, index) => (
              <div key={index} className={styles.btn_container}>
                <Typography fontSize={16}>Кнопка {index}</Typography>
                <TextField
                  value={chosenButton.text}
                  onChange={(event) =>
                    setChosenButtons(
                      produce((draft) => {
                        draft[index].text = event.target.value;
                      })
                    )
                  }
                />
                <FormControlLabel
                  label={
                    chosenButton.type === 'fast-response'
                      ? 'Быстрый ответ'
                      : 'Ссылка'
                  }
                  control={
                    <Switch
                      value={chosenButton.type === 'link'}
                      onChange={(event) =>
                        setChosenButtons(
                          produce((draft) => {
                            draft[index].type = event.target.checked
                              ? 'link'
                              : 'fast-response';
                          })
                        )
                      }
                    />
                  }
                />
              </div>
            ))}
          </Stack>
          <Stack gap={3}>
            <Button
              endIcon={<Add />}
              variant="contained"
              onClick={() =>
                setChosenButtons(
                  produce((draft) => {
                    draft.push({
                      text: '',
                      type: 'fast-response',
                    });
                  })
                )
              }
            >
              Добавить кнопку
            </Button>
            <Button
              color="success"
              endIcon={<CheckSharp />}
              variant="contained"
              onClick={() => {
                setModalOpen(false);
                setButtons?.(chosenButtons);
              }}
            >
              Готово
            </Button>
          </Stack>
        </Box>
      </Modal>
    </>
  );
}

export default ButtonsPicker;
