# Тестовое задание на позицию Fullstack разработчик (стажер): Приложение Fromni

## Установка 

Скачайте репозиторий:
```bash
git clone https://gitlab.com/krivenko_maxim/eba-test-task.git
```

## Конфигурация

Создайте файл `.env` со следующей структурой:

```bash
POSTGRES_USER='default'
POSTGRES_PASSWORD='default'
POSTGRES_DB='default'
```

или выполните: 

```bash
cp .env.dev .env
```

## Запуск

### Docker

Используя docker compose:

```bash
docker compose up --build
```

Перейдите в браузере по [ссылке](http://localhost).