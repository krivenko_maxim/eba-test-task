import { ChannelType, Message } from '../types/message';

const defaultMessageMetadata: Message[] = [
  {
    type: 'VK',
    keyboardType: 'inline',
    text: '',
    buttons: [],
  },
  {
    type: 'Telegram',
    keyboardType: 'inline',
    text: '',
    buttons: [],
  },
  {
    type: 'SMS',
    text: '',
  },
  {
    type: 'WhatsApp',
    keyboardType: 'inline',
    text: '',
    buttons: [],
  },
];

const keyboardAvailable: Record<ChannelType, boolean> = {
  SMS: false,
  VK: true,
  Telegram: true,
  WhatsApp: true,
};

export { defaultMessageMetadata, keyboardAvailable };
