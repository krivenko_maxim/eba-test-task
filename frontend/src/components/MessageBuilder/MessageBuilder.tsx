import {
  TextField,
  Typography,
  Stack,
  Switch,
  FormControlLabel,
  Container,
} from '@mui/material';
import { KeyboardButton, KeyboardType } from '../../types/message';
import styles from './MessageBuilder.module.css';
import ButtonsPicker from '../ButtonsPicker/ButtonsPicker';

interface MessageBuilderProps {
  channel: string;
  messageText?: string;
  onChangeMessage?: (newMessage: string) => void;
  keyboardAvailable: boolean;
  keyboardType?: KeyboardType;
  setKeyboardType?: (type: KeyboardType) => void;
  disabled: boolean;
  buttons?: KeyboardButton[];
  setButtons?: (newButtons: KeyboardButton[]) => void;
}

function MessageBuilder({
  channel,
  messageText,
  onChangeMessage,
  keyboardAvailable = true,
  keyboardType,
  setKeyboardType,
  disabled,
  buttons,
  setButtons,
}: MessageBuilderProps) {
  return (
    <Stack className={styles.container}>
      <Typography>{channel}</Typography>
      <TextField
        disabled={disabled}
        placeholder="Введите текст сообщения"
        value={messageText}
        onChange={(event) => onChangeMessage?.(event.target.value)}
      />
      <Container>
        {keyboardAvailable ? (
          <>
            <Typography>Тип клавиатуры</Typography>
            <FormControlLabel
              disabled={disabled}
              className={styles.kb_type}
              control={
                <Switch
                  value={keyboardType === 'standard'}
                  onChange={(event) =>
                    setKeyboardType?.(
                      event.target.checked ? 'standard' : 'inline'
                    )
                  }
                />
              }
              label={keyboardType}
            />
            {buttons && (
              <ButtonsPicker buttons={buttons} setButtons={setButtons} />
            )}
          </>
        ) : (
          <Typography>Для выбранного канала клавиатура недоступна.</Typography>
        )}
      </Container>
    </Stack>
  );
}

export default MessageBuilder;
