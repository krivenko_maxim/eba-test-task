import { FormHelperText, TextField } from '@mui/material';

interface UsernamePickerProps {
  username: string;
  onChangeUsername: (newUsername: string) => void;
}

function UsernamePicker({ username, onChangeUsername }: UsernamePickerProps) {
  return (
    <TextField
      label="Имя пользователя"
      placeholder="Введите имя пользователя"
      value={username}
      onChange={(e) => onChangeUsername(e.target.value)}
    />
  );
}

export default UsernamePicker;
