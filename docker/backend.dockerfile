FROM node:20-alpine

WORKDIR /usr/app/backend

COPY ./backend/package.json .

RUN npm install

COPY ./backend .

CMD [ "npm", "run", "start" ]