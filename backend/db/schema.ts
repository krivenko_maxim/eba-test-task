import {
  AllowNull,
  BelongsTo,
  Column,
  ForeignKey,
  HasMany,
  Model,
  NotNull,
  Table,
  Unique,
} from "sequelize-typescript";
import { sequelize } from "./init.js";

@Table({
  tableName: "message",
})
class Message extends Model {
  @Column
  type: number;

  @ForeignKey(() => MessageType)
  @Column
  message_type_id: number;

  @BelongsTo(() => MessageType)
  message_type: ReturnType<() => MessageType>;

  @ForeignKey(() => User)
  @Column
  user_id: number;

  @BelongsTo(() => User)
  user: ReturnType<() => User>;

  @Column
  text: string;

  @Column
  keyboard_type: string;
}

@Table({
  modelName: "message_type",
})
class MessageType extends Model {
  @Column
  message_type: string;

  @HasMany(() => Message)
  messages: Message[];
}

@Table({
  modelName: "user",
})
class User extends Model {
  @Unique
  @AllowNull(false)
  @Column
  username: string;

  @HasMany(() => Message)
  messages: Message[];
}

@Table({
  modelName: "keyboard_button",
})
class KeyboardButton extends Model {
  @Column
  text: string;

  @ForeignKey(() => Message)
  @Column
  message_id: number;

  @BelongsTo(() => Message)
  message: Message;
}

sequelize.addModels([Message, User, MessageType, KeyboardButton]);

export { Message, User, MessageType, KeyboardButton };
